require 'open-uri'
require 'open_uri_redirections'
require 'action_view/helpers/asset_tag_helper'
require 'nokogiri'
require 'uri'
require 'base64'

class ProxyController < ApplicationController
  def fetch
    response.headers['Access-Control-Allow-Origin'] = '*'

    # Open url
    url = URI.encode(params[:url])
    html = nil
    @ua = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36"
    begin
      open(url, allow_redirections: :all, 'User-Agent' => @ua) do |io|
        if io.content_type != 'text/html'
          encoded = Base64.encode64 io.read
          body ="<img src='data:#{io.content_type};base64,#{encoded}' />"
          # Add hidden div with metadata
          title = url.match(/.*\/(.*)/).captures[0]
          body = "<div style='display:none'>
   				<div id='contentTitle'>#{title}</div>
   				<div id='contentURL'>#{url}</div>
   			</div>" + body
          render text: body
  	  return
        end
        html = io.read.mb_chars.tidy_bytes
      end
    rescue OpenURI::HTTPError => e
      render text: e.to_s
      return
    end

    title = ""
    if match = html.match(/<title>(.*)<\/title>/im)
      title = match.captures[0]
    else
      title = url
    end
    
    html.gsub! /\[/, "("
    html.gsub! /\]/, ")"

    @magic_number = "12345678901234567890098765743210987654321"
    html = focus_content(html, url)
    
    # Write to file
    unfiltered_filename = "unfiltered.html"
    open(unfiltered_filename, "w") do |file|
      file.puts html
    end
    
    # Double loading of url since Lynx parses links correctly but we need to clean up the body
    unfiltered_body, unfiltered_references = parse_url(unfiltered_filename)
    original_body, references = parse_url(url)
    
    filtered_body = if match = unfiltered_body.match(/^.*#{@magic_number}(.*)#{@magic_number}.*$/m)
                      match.captures[0]
                    else
                      unfiltered_body
                    end
    
    # Determine prefix
    prefix_hash = Hash.new(0)
    good_urls = parse_references(references).map {|x,y| y}
    parse_references(unfiltered_references).each do |number, url|
      if (match = url.match(/^file:\/\/(.*)/))
	url = match.captures[0]
	found_url = good_urls.find do |good_url|
	  good_url.include? url
	end
        if found_url
          match = found_url.match /^(.*)#{Regexp.quote(url)}/
          prefix = match.captures[0]
	  prefix_hash[prefix] += 1
        end
      end
    end
    
    max = 0
    max_prefix = ""
    prefix_hash.each_pair do |prefix, value|
      if (value > max)
    	max = value
    	max_prefix = prefix
      end
    end

    unfiltered_references.gsub! /file:\/\//, max_prefix
    
    body = replace_references(url, filtered_body, unfiltered_references)
    
    # replace newlines with br
    body.gsub! /\n/, '<br />'

    # Add hidden div with metadata
    body = "<div style='display:none'>
   				<div id='contentTitle'>#{title}</div>
   				<div id='contentURL'>#{url}</div>
   			</div>" + body

    render text: body
  end
  
  private
  def focus_content(html, url)
    {
      'www.webmd.com/search/search_results/default.aspx?query=' => '#mainContentContainer_area',
      'www.google.com/search?q=' => '#center_col',
      'wikipedia.org/wiki' => '#content',
      'www.mylan.com/en/search?q=' => 'div.container-full.padded-content',
      'www.bing.com/search?q=' => '#b_results',
      'www.mayoclinic.org/search/search-results?q=' => 'div#main-content div.results'
    }.each_pair do |host, css|
      if url.include? host
        noko = Nokogiri::HTML(html)
        element = noko.at_css css
        if element
          element.add_previous_sibling("<span>#{@magic_number}</span>")
          element.add_next_sibling("<span>#{@magic_number}</span>")
          html = noko.to_html
        end
      end
    end
    html
  end
  
  def replace_references(url, body, references)
    # parse references into array of [number, url] pairs
    urls = parse_references(references)
      
    # replace numbered references with links
    urls.each do |number, this_url|
      if url.match(/^javascript/)
    	body.gsub! "[#{number}]", ""
      else
        body.gsub! /\[#{number}\]/, " <a href='#{this_url}'><img src='#{request.protocol}#{request.host_with_port}#{ActionController::Base.helpers.image_url 'link.png'}'></img></a>"
      end
    end
    body
  end

  def parse_references(references)
    if references == nil
      []
    else
      references.split("\n").map do |numbered_url|
    	match = numbered_url.match(/ *([0-9]*)\. (.*)/)
    	if match
    	  match.captures
    	else
    	  nil
    	end
      end.select {|x| x != nil}
    end
  end

  def parse_url(param)
    # fetch text dump
    text = `lynx -dump -image_links -useragent='#{@ua}' #{param}`
    # fix non UTF-8 characters
    text = text.mb_chars.tidy_bytes

    # split body and references
    match = text.match(/(.*)\n\nReferences\n\n(.*)/m)
    if match != nil
      match.captures
    else
      [text, nil]
    end
  end
end
