This was for the 2016 Mylan Hackathon.

This WebApp aims at reducing the amount of bandwidth required to gain access to pertinent information on the internet. This app is targeted toward developing areas that are coming online for the first time, for clients who may 'pay per kilobyte' for their data usage.  

Lightweight HTML/CSS/JS webapp
Ruby on Rails server
